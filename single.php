<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header();
?>


<?php while (have_posts()) : the_post(); ?>

	<div id="content">
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="post-infos"><?php the_time(__('F jS, Y', 'my-tapestry')) ?> &bull; <?php the_time(__('H:m', 'my-tapestry')) ?></div>
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<!-- desbest edit -->
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-	-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" class="postthumbnail">
			<?php endif; ?>
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages', 'my-tapestry').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		</div>
	
		<?php comments_template(); ?>
	</div>

	<div id="sidebar" class="fixed">
		<ul class="post-infos">
			<li><span><?php _e('By', 'my-tapestry'); ?></span> <?php the_author() ?></li>
			<li><span><?php _e('Posted in', 'my-tapestry'); ?></span> <?php the_category(', ') ?></li>
			<?php the_tags('<li><span>'.__('Tagged', 'my-tapestry').'</span> ', ' ', '</li>'); ?>
			<?php edit_post_link(__('Edit this post', 'my-tapestry'), '<li>', '</li>'); ?>
		</ul>
	</div>

<?php endwhile; ?>

<?php get_footer(); ?>