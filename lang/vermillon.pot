msgid ""
msgstr ""
"Project-Id-Version: Vermillon\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-02-08 17:46+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: Benoît Burgener <contact@benoitburgener.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:11
msgid "Error 404 - Not Found"
msgstr ""

#: archive.php:16
msgid "Category"
msgstr ""

#: archive.php:18
msgid "Posts Tagged"
msgstr ""

#: archive.php:20
#: archive.php:22
#: archive.php:24
#: archive.php:26
msgid "Archive"
msgstr ""

#: archive.php:20
#: archive.php:34
#: image.php:15
#: index.php:16
#: search.php:25
#: single.php:15
msgid "F jS, Y"
msgstr ""

#: archive.php:22
msgid "F, Y"
msgstr ""

#: archive.php:28
msgid "Blog Archives"
msgstr ""

#: archive.php:35
#: functions.php:34
#: image.php:16
#: index.php:17
#: search.php:29
#: single.php:16
msgid "Permanent Link to"
msgstr ""

#: archive.php:36
#: image.php:19
#: index.php:18
#: page.php:25
#: search.php:30
msgid "Continue reading"
msgstr ""

#: archive.php:37
#: image.php:25
#: index.php:19
#: search.php:33
#: single.php:26
msgid "By"
msgstr ""

#: archive.php:37
#: image.php:26
#: index.php:19
#: search.php:33
#: single.php:27
msgid "Posted in"
msgstr ""

#: archive.php:37
#: comments.php:20
#: index.php:19
#: search.php:33
msgid "No Comment"
msgstr ""

#: archive.php:37
#: comments.php:20
#: index.php:19
#: search.php:33
msgid "1 Comment"
msgstr ""

#: archive.php:37
#: comments.php:20
#: index.php:19
#: search.php:33
msgid "% Comments"
msgstr ""

#: archive.php:37
#: functions.php:59
#: index.php:19
#: search.php:33
msgid "Edit"
msgstr ""

#: archive.php:44
#: index.php:26
#: search.php:41
msgid "Older Entries"
msgstr ""

#: archive.php:45
#: index.php:27
#: search.php:42
msgid "Newer Entries"
msgstr ""

#: archive.php:52
#, php-format
msgid "Sorry, but there aren't any posts in the %s category yet."
msgstr ""

#: archive.php:54
msgid "Sorry, but there aren't any posts with this date."
msgstr ""

#: archive.php:57
#, php-format
msgid "Sorry, but there aren't any posts by %s yet."
msgstr ""

#: archive.php:59
msgid "No posts found."
msgstr ""

#: comments.php:12
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: comments.php:21
msgid "Ping"
msgstr ""

#: comments.php:38
msgid "Be the first to respond!"
msgstr ""

#: comments.php:47
msgid "Leave a Reply"
msgstr ""

#: comments.php:54
msgid "You must be"
msgstr ""

#: comments.php:54
msgid "logged in"
msgstr ""

#: comments.php:54
msgid "to post a comment"
msgstr ""

#: comments.php:59
msgid "Logged in as"
msgstr ""

#: comments.php:59
msgid "Log out of this account"
msgstr ""

#: comments.php:59
msgid "Log out"
msgstr ""

#: comments.php:63
msgid "Name"
msgstr ""

#: comments.php:63
#: comments.php:66
msgid "(required)"
msgstr ""

#: comments.php:66
msgid "Mail"
msgstr ""

#: comments.php:66
msgid "(will not be published)"
msgstr ""

#: comments.php:69
msgid "Website"
msgstr ""

#: comments.php:73
msgid "Submit Comment"
msgstr ""

#: comments.php:82
msgid "Sorry, comments are closed."
msgstr ""

#: footer.php:13
msgid "Powered by"
msgstr ""

#: footer.php:17
#: header.php:62
msgid "Entries"
msgstr ""

#: footer.php:17
#: header.php:62
msgid "Comments"
msgstr ""

#: footer.php:18
msgid "Top"
msgstr ""

#: functions.php:57
msgid "Trackback"
msgstr ""

#: functions.php:57
msgid "Pingback"
msgstr ""

#: functions.php:58
msgid "M jS Y"
msgstr ""

#: functions.php:58
#: image.php:15
#: single.php:15
msgid "H:m"
msgstr ""

#: functions.php:58
msgid "by"
msgstr ""

#: header.php:43
msgid "Categories"
msgstr ""

#: header.php:49
#: header.php:67
msgid "Archives"
msgstr ""

#: header.php:55
msgid "Tags"
msgstr ""

#: header.php:59
msgid "Search"
msgstr ""

#: header.php:61
msgid "Subscribe"
msgstr ""

#: image.php:27
#: single.php:28
msgid "Tagged"
msgstr ""

#: image.php:28
#: single.php:29
msgid "Edit this post"
msgstr ""

#: index.php:33
msgid "Not Found"
msgstr ""

#: index.php:34
msgid "Sorry, but you are looking for something that isn't here."
msgstr ""

#: page.php:26
#: single.php:18
msgid "Pages"
msgstr ""

#: page.php:29
msgid "Edit this page"
msgstr ""

#: search.php:14
msgid "Search Results"
msgstr ""

#: search.php:48
msgid "No posts found. Try a different search?"
msgstr ""

#: sidebar.php:17
msgid "Meta"
msgstr ""

