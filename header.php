<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie.css" media="screen" /><![endif]-->
		
		<!-- outdated in newer versions of wordpress -->
		<!-- <link rel="pingback" href="<?php //bloginfo('pingback_url'); ?>" /> -->
		
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
		<?php wp_head(); ?>
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	</head>
	
	<body <?php body_class(); ?>>
		
		<div id="wrapper">
			
			<div id="header">
				<div class="box">
					<div class="left">
						<h1><a href="<?php echo get_option('home'); ?>/" class="home"><?php bloginfo('name'); ?></a></h1>
						<p><?php bloginfo('description'); ?></p>
					</div>
					<div class="right">
						<?php //wp_page_menu(array('menu_class' => 'nav')); ?>
							<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'nav' ) ); ?>
					</div>
				</div>
			</div>

			<div id="archives" style="<?php if(!is_404() && (!is_search() || have_posts())) echo 'display:none'; ?>">
				<div class="box">
	    			<div class="col">
	    				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('far-left') ) : ?>
	    				<h2><?php _e('Categories', 'my-tapestry'); ?></h2>
	    				<ul>
							<?php wp_list_categories(array('title_li' => '')); ?>
	    				</ul>
	    				<?php endif; ?>
	    			</div>
	    			<div class="col">
	    				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('center-left') ) : ?>
	    				<h2><?php _e('Archives', 'my-tapestry'); ?></h2>
	    				<ul>
	    					<?php wp_get_archives(); ?>
	    				</ul>
	    				<?php endif; ?>
	    			</div>
	    			<div class="col">
	    				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('center-right') ) : ?>
	    				<h2><?php _e('Tags', 'my-tapestry'); ?></h2>
	    				<?php wp_tag_cloud(); ?>
	    				<?php endif; ?>
	    			</div>
	    			<div class="col">
	    				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('far-right') ) : ?>
	    				<h2><?php _e('Search', 'my-tapestry'); ?></h2>
						<?php get_search_form(); ?>
						<h2 style="margin-top: 40px"><?php _e('Subscribe', 'my-tapestry'); ?></h2>
						<p class="rss"><a href="<?php bloginfo('rss2_url'); ?>"><?php _e('Entries', 'my-tapestry'); ?></a> <a href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e('Comments', 'my-tapestry'); ?></a></p>
						<?php endif; ?>
	    			</div>
    			</div>
    		</div>
    		
    		<div id="toggleArchives" onclick="toggleArchives();">&darr; <?php _e('Menu', 'my-tapestry'); ?> &darr;</div>
				
			<div id="page">
				<div class="box">
