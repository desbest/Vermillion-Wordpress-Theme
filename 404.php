<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header();
?>

<div id="content">
	<h2><?php _e('Error 404 - Not Found', 'my-tapestry'); ?></h2>
</div>

<?php get_footer(); ?>