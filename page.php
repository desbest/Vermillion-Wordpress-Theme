<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header(); ?>

<div id="content">

	<?php
	$parent_id = get_post_ancestors($post->ID);
	if ($parent_id[0]) $parent = wp_list_pages(array('title_li' => '', 'include' => $parent_id[0], 'echo' => '0', 'link_before' => '&larr; '));
	$children = wp_list_pages('title_li=&depth=1&child_of='.$post->ID.'&echo=0');
	if (!empty($parent) || !empty($children)) : ?>
		<ul class="nav">
			<?php if (!empty($parent)) echo $parent; ?>
			<?php if (!empty($children)) echo $children; ?>
		</ul>
	<?php endif; ?>
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div <?php post_class('post') ?> id="post-<?php the_ID(); ?>">
			<h2><?php the_title(); ?></h2>
			<?php the_content(__('Continue reading', 'my-tapestry').' &rarr;'); ?>
			<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages', 'my-tapestry').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		</div>
	<?php endwhile; endif; ?>
	<?php edit_post_link(__('Edit this page', 'my-tapestry'), '<p>', '</p>'); ?>

	<?php comments_template(); ?>
	
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>