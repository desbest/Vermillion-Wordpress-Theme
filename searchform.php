<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */
?>
<form class="searchform" method="get" action="<?php bloginfo('url'); ?>">
	<p class="clear"><input type="text" name="s" class="s" size="30" value="<?php the_search_query(); ?>" /><input type="submit" class="searchSubmit" value="Search" /></p>
</form>