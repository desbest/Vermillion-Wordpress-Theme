<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header(); ?>

<div id="content">

<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="post-infos"><?php the_time(__('F jS, Y', 'my-tapestry')) ?></div>
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<!-- desbest edit -->
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-	-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" class="postthumbnail">
			<?php endif; ?>
			<?php the_content(__('Continue reading', 'my-tapestry').' &rarr;'); ?>
			<div class="post-infos"><?php _e('By', 'my-tapestry'); ?> <?php the_author() ?> &bull; <?php _e('Posted in', 'my-tapestry'); ?> <?php the_category(', ') ?> &bull; <?php comments_popup_link(__('No Comment', 'my-tapestry'), __('1 Comment', 'my-tapestry'), __('% Comments', 'my-tapestry')); ?> <?php edit_post_link(__('Edit', 'my-tapestry'), '&bull; ', ''); ?></div>
		</div>

	<?php endwhile; ?>
	
	<?php if(show_posts_nav()): ?>
	<div class="navigation clear">
		<div class="left"><?php next_posts_link('&larr; '.__('Older Entries', 'my-tapestry')) ?></div>
		<div class="right"><?php previous_posts_link(__('Newer Entries', 'my-tapestry').' &rarr;') ?></div>
	</div>
	<?php endif; ?>

<?php else : ?>

	<h2><?php _e('Not Found', 'my-tapestry'); ?></h2>
	<p><?php _e("Sorry, but you are looking for something that isn't here.", 'my-tapestry'); ?></p>
	<?php get_search_form(); ?>

<?php endif; ?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>