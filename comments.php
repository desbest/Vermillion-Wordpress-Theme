<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="alert"><?php _e('This post is password protected. Enter the password to view comments.', 'my-tapestry'); ?></p>
	<?php
		return;
	}
?>

<div id="comments">
	<div class="clear title">
		<div class="left"><h2><?php comments_number(__('No Comment', 'my-tapestry'), __('1 Comment', 'my-tapestry'), __('% Comments', 'my-tapestry'));?></h2></div>
		<div class="right"><?php if (pings_open($post_id)) echo '<a href="'.trackback_url(false).'" class="arrow">'.__('Ping', 'my-tapestry').'</a>'; ?> <span class="rss"><?php post_comments_feed_link('RSS'); ?></span></div>
	</div>

<?php if ( have_comments() ) : ?>
    <div id="commentlist">
    	<?php wp_list_comments(array('callback' => 'vermillon_comment', 'style' => 'li')); ?>
    </div>

	<div class="navigation clear">
		<div class="left"><?php previous_comments_link() ?></div>
		<div class="right"><?php next_comments_link() ?></div>
	</div>
	
<?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		
	<p><?php _e('Be the first to respond!', 'my-tapestry'); ?></p>
		
	<?php endif; ?>
<?php endif; ?>
</div>


<div id="respond">

	<h2><?php comment_form_title(__('Leave a Reply', 'my-tapestry')); ?></h2>

<?php if ( comments_open() ) : ?>

	<p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>

	<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
	<p><?php _e('You must be', 'my-tapestry'); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'my-tapestry'); ?></a> <?php _e('to post a comment', 'my-tapestry'); ?>.</p>
	<?php else : ?>

	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( is_user_logged_in() ) : ?>
		<p><?php _e('Logged in as', 'my-tapestry'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'my-tapestry'); ?>"><?php _e('Log out', 'my-tapestry'); ?> &rarr;</a></p>

		<?php else : ?>
		<p><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" />
		<label for="author"><?php _e('Name', 'my-tapestry'); ?> <?php if ($req) _e('(required)', 'my-tapestry'); ?></label></p>

		<p><input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" />
		<label for="email"><?php _e('Mail', 'my-tapestry'); ?> <?php _e('(will not be published)', 'my-tapestry'); ?> <?php if ($req) _e('(required)', 'my-tapestry'); ?></label></p>

		<p><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
		<label for="url"><?php _e('Website', 'my-tapestry'); ?></label></p>
		<?php endif; ?>

		<p><textarea name="comment" id="comment" cols="64" rows="10" tabindex="4"></textarea></p>
		<p><input name="submit" type="submit" id="submit" tabindex="5" class="submit" value="<?php _e('Submit Comment', 'my-tapestry'); ?>" /></p>

		<?php comment_id_fields(); ?>
		<?php do_action('comment_form', $post->ID); ?>
	</form>

	<?php endif; // If registration required and not logged in ?>

<?php else : // comments are closed ?>
	<p class="nocomments"><?php _e('Sorry, comments are closed.', 'my-tapestry'); ?></p>

<?php endif; // if you delete this the sky will fall on your head ?>

</div>