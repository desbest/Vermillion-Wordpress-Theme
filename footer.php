<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */
?>
				</div>
			</div> <!-- END #page -->

			<div id="footer">
				<div class="box">
					<div class="left">
						<p><?php bloginfo('name'); ?> &copy; <?php echo date("Y"); ?> &bull; <?php _e('Powered by', 'my-tapestry'); ?> <a href="http://wordpress.org/">WordPress</a></p>
						<p>Theme "Vermillon" by <a href="http://www.benoitburgener.com">Beno&icirc;t &laquo;LeBen&raquo; Burgener</a> for <a href="http://www.my-tapestry.com">My Tapestry</a></p>
					</div>
					<div class="right rss">
						<p><a href="<?php bloginfo('rss2_url'); ?>"><?php _e('Entries', 'my-tapestry'); ?></a> <a href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e('Comments', 'my-tapestry'); ?></a></p>
						<p><a href="#wrapper" class="arrow"><?php _e('Top', 'my-tapestry'); ?></a></p>
					</div>
				</div>
			</div>
		</div>
	<?php wp_footer(); ?>
	</body>

</html>