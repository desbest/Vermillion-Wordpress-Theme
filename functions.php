<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

add_theme_support('automatic-feed-links');
add_theme_support('post-thumbnails');

function register_my_menu() {
register_nav_menu('main-menu',__( 'New Menu' ));
}
add_action( 'init', 'register_my_menu' );

load_theme_textdomain('my-tapestry', TEMPLATEPATH.'/lang/');

function myscripts(){
	wp_enqueue_script('theme_functions', get_bloginfo('template_url').'/js/functions.js', array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'myscripts' );

function show_posts_nav() {
    global $wp_query;
    return ($wp_query->max_num_pages > 1);
}

function related_posts($post_id, $number) {
	$tags = wp_get_post_tags($post_id);
	if ($tags) {
		echo '<div class="related-posts">';
		echo '<h3>Related Posts</h3>';
		$first_tag = $tags[0]->term_id;
		$args=array(
			'tag__in' => array($first_tag),
			'post__not_in' => array($post_id),
			'showposts' => $number,
			'caller_get_posts' => 1
		 );
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) {
			echo '<ul>';
			while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
				<?php
			endwhile;
			echo '</ul>';
		}
		echo '</div>';
	}
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'id' => 'sidebar',
		'name' => 'Far Right Expansion Block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'far-left',
		'name' => 'Far Left Expansion Block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'center-left',
		'name' => 'Center Left Expansion Block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'center-right',
		'name' => 'Center Right Expansion Block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'far-right',
		'name' => 'Far Right Expansion Block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));

}

function vermillon_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
   	<li class="clear" id="comment-<?php comment_ID() ?>">
    	<div <?php comment_class('clear'); ?>>
    		<div class="left">
    			<?php comment_type('', '<p>'.__('Trackback', 'my-tapestry').'</p>', '<p>'.__('Pingback', 'my-tapestry').'</p>'); ?>
    			<p><small><strong><?php comment_date(__('M jS Y', 'my-tapestry')) ?></strong> &bull; <a href="#comment-<?php comment_ID() ?>"><?php comment_time(__('H:m', 'my-tapestry')) ?></a></small><br /><?php _e('by', 'my-tapestry'); ?> <strong><?php comment_author_link() ?></strong></p>
    			<p><?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?><?php edit_comment_link(__('Edit', 'my-tapestry'), ' &bull; ', ''); ?></p>
    		</div>
    		
    		<div class="right">
	    		<?php echo get_avatar( $comment, 50 ); ?>
    			<?php comment_text() ?>
    		</div>
    	</div>
<?php
}
?>