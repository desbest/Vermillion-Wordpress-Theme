# Vermillion wordpress theme

* [Originally created by Benoit Burgener](https://www.benoitburgener.ch/)
* [outdated download](https://wordpress.org/themes/vermillon/) [updated download](https://gitlab.com/desbest/Vermillion-Wordpress-Theme)
* [Updated by desbest](http://desbest.com)

![vermillion screenshot](https://i.imgur.com/9X3i9Vy.png)
