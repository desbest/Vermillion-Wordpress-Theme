<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header(); ?>

<div id="content">

	<?php if (have_posts()) : ?>

	<div class="title clear">
		<div class="left"><h2><?php _e('Search Results', 'my-tapestry'); ?></h2></div>
		<div class="right"><?php get_search_form(); ?></div>
	</div>

	<?php while (have_posts()) : the_post(); ?>

		<div <?php post_class('post') ?> id="post-<?php the_ID(); ?>">
			<div class="post-infos">
			<?php if($post->post_type == 'page'):
				echo 'Page';
			else:
				the_time(__('F jS, Y', 'my-tapestry'));
			endif; ?>
			</div>
			
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<?php the_content(__('Continue reading', 'my-tapestry').' &rarr;'); ?>

			<?php if($post->post_type != 'page'): ?>
			<div class="post-infos"><?php _e('By', 'my-tapestry'); ?> <?php the_author() ?> &bull; <?php _e('Posted in', 'my-tapestry'); ?> <?php the_category(', ') ?> &bull; <?php comments_popup_link(__('No Comment', 'my-tapestry'), __('1 Comment', 'my-tapestry'), __('% Comments', 'my-tapestry')); ?> <?php edit_post_link(__('Edit', 'my-tapestry'), '&bull; ', ''); ?></div>
			<?php endif; ?>
		</div>

	<?php endwhile; ?>
	
	<?php if(show_posts_nav()): ?>
	<div class="navigation clear">
		<div class="left"><?php next_posts_link('&larr; '.__('Older Entries', 'my-tapestry')) ?></div>
		<div class="right"><?php previous_posts_link(__('Newer Entries', 'my-tapestry').' &rarr;') ?></div>
	</div>
	<?php endif; ?>

	<?php else : ?>

		<h2><?php _e('No posts found. Try a different search?', 'my-tapestry'); ?></h2>

	<?php endif; ?>

</div>

<?php if (have_posts()) : ?>
	<?php get_sidebar(); ?>
<?php endif; ?>
<?php get_footer(); ?>