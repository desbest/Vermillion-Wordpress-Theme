<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */
?>
<div id="sidebar">

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar') ) : ?>
   		<div class="widget">
   			<ul>
   			<?php wp_list_bookmarks('title_li=&title_before=<h3>&title_after=</h3>'); ?>
   			</ul>
   		</div>
   		
   		<div class="widget">
      			<h3><?php _e('Meta', 'my-tapestry'); ?></h3>
      			<ul>
      				<?php wp_register(); ?>
      				<li><?php wp_loginout(); ?></li>
      				<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
      				<?php wp_meta(); ?>
      			</ul>
   		</div>
	<?php endif; ?>

</div>