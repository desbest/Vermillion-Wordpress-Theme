<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header();
?>


<?php while (have_posts()) : the_post(); ?>

	<div id="content">
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="post-infos"><?php the_time(__('F jS, Y', 'my-tapestry')) ?> &bull; <?php the_time(__('H:m', 'my-tapestry')) ?></div>
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<p><a href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a></p>
			<?php if ( !empty($post->post_excerpt) ) the_excerpt(); // this is the "caption" ?>
			<?php the_content(__('Continue reading', 'my-tapestry').' &rarr;'); ?>
		</div>
	</div>

	<div id="sidebar" class="fixed">
		<ul class="post-infos">
			<li><span><?php _e('By', 'my-tapestry'); ?></span> <?php the_author() ?></li>
			<li><span><?php _e('Posted in', 'my-tapestry'); ?></span> <?php the_category(', ') ?></li>
			<?php the_tags('<li><span>'.__('Tagged', 'my-tapestry').'</span> ', ' ', '</li>'); ?>
			<?php edit_post_link(__('Edit this post', 'my-tapestry'), '<li>', '</li>'); ?>
		</ul>
	</div>

<?php endwhile; ?>

<?php get_footer(); ?>