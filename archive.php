<?php
/**
 * @package WordPress
 * @subpackage Vermillon
 */

get_header();
?>

<div id="content">

	<?php if (have_posts()) : ?>

	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
	<?php /* If this is a category archive */ if (is_category()) { ?>
	<h2><?php _e('Category', 'my-tapestry'); ?> &rarr; <?php single_cat_title(); ?></h2>
	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
	<h2><?php _e('Posts Tagged', 'my-tapestry'); ?> &rarr; <?php single_tag_title(); ?></h2>
	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
	<h2><?php _e('Archive', 'my-tapestry'); ?> &rarr; <?php the_time(__('F jS, Y', 'my-tapestry')); ?></h2>
	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<h2><?php _e('Archive', 'my-tapestry'); ?> &rarr; <?php the_time(__('F, Y', 'my-tapestry')); ?></h2>
	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<h2><?php _e('Archive', 'my-tapestry'); ?> &rarr; <?php the_time('Y'); ?></h2>
	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
	<h2><?php _e('Archive', 'my-tapestry'); ?> &rarr; Author</h2>
	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
	<h2><?php _e('Blog Archives', 'my-tapestry'); ?></h2>
	<?php } ?>

	<?php while (have_posts()) : the_post(); ?>
			
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="post-infos"><?php the_time(__('F jS, Y', 'my-tapestry')) ?></div>
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'my-tapestry'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<?php the_content(__('Continue reading', 'my-tapestry').' &rarr;'); ?>
			<div class="post-infos"><?php _e('By', 'my-tapestry'); ?> <?php the_author() ?> &bull; <?php _e('Posted in', 'my-tapestry'); ?> <?php the_category(', ') ?> &bull; <?php comments_popup_link(__('No Comment', 'my-tapestry'), __('1 Comment', 'my-tapestry'), __('% Comments', 'my-tapestry')); ?> <?php edit_post_link(__('Edit', 'my-tapestry'), '&bull; ', ''); ?></div>
		</div>

	<?php endwhile; ?>

	<?php if(show_posts_nav()): ?>
	<div class="navigation clear">
		<div class="left"><?php next_posts_link('&larr; '.__('Older Entries', 'my-tapestry')) ?></div>
		<div class="right"><?php previous_posts_link(__('Newer Entries', 'my-tapestry').' &rarr;') ?></div>
	</div>
	<?php endif; ?>

	<?php else :

		if ( is_category() ) { // If this is a category archive
			printf('<p>'.__("Sorry, but there aren't any posts in the %s category yet.", 'my-tapestry').'</p>', single_cat_title('',false));
		} else if ( is_date() ) { // If this is a date archive
			echo '<p>'.__("Sorry, but there aren't any posts with this date.", 'my-tapestry').'</p>';
		} else if ( is_author() ) { // If this is a category archive
			$userdata = get_userdatabylogin(get_query_var('author_name'));
			printf('<p>'.__("Sorry, but there aren't any posts by %s yet.", 'my-tapestry').'</p>', $userdata->display_name);
		} else {
			echo '<p>'.__("No posts found.", 'my-tapestry').'</p>';
		}
		get_search_form();

	endif;
?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>